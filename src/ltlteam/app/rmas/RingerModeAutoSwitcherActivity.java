package ltlteam.app.rmas;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RingerModeAutoSwitcherActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button startButton = (Button) findViewById(R.id.startButton);
		Button stopButton = (Button) findViewById(R.id.stopButton);
		Button settingButton = (Button) findViewById(R.id.settingButton);
		
		stepView = (TextView) findViewById(R.id.stepView);

		startButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new Runnable() {

					@Override
					public void run() {
						Intent intent = new Intent(
								RingerModeAutoSwitcherActivity.this,
								JudgeService.class);
						startService(intent);
					}
				}).start();
			}
		});

		stopButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new Runnable() {

					@Override
					public void run() {
						Intent intent = new Intent(
								RingerModeAutoSwitcherActivity.this,
								JudgeService.class);
						stopService(intent);
					}
				}).start();
			}
		});
		
		settingButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(
						RingerModeAutoSwitcherActivity.this, 
						SettingListAcitivty.class);
				startActivity(intent);
			}
		});

//		handler = new Handler() {
//			public void handleMessage(Message msg) {
//				super.handleMessage(msg);
//
//			}
//		};

		receiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				stepView.setText(String.valueOf(intent.getIntExtra("step", 0)));
			}
		};
		IntentFilter filter = new IntentFilter(
				"zossin.app.rmas.RingerModeAutoSwitcherActivity");
		registerReceiver(receiver, filter);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		unregisterReceiver(receiver);
	}

	private BroadcastReceiver receiver = null;
	private TextView stepView;
//	private Handler handler;
//	private static final String TAG = "RingerModeAutoSwitcherActivity";

}