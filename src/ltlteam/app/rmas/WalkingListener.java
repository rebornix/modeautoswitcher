package ltlteam.app.rmas;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public final class WalkingListener implements SensorEventListener {
	public WalkingListener(JudgeService parent) {
		this.parent = parent;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			analyseData(event.values, event.timestamp);
		}
	}
	
	public void analyseData(float[] values, long timestamp) {
		if (timestamp - timePrevious >= 200000000L || timePrevious < 0) {
			if (preAccData == null) {
				preAccData = new float[3];
				System.arraycopy(values, 0, preAccData, 0, 3);
			}
			else {
				if (calculateAngle(values, preAccData) >= WALKING_THRESHOLD) {
					parent.addStep();
				}
				System.arraycopy(values, 0, preAccData, 0, 3);
			}
			timePrevious = timestamp;
		}
	}
	
	public float calculateAngle(float[] newPoint, float[] oldPoint) {
		float vectorProduct = 0F, newMold = 0F, oldMold = 0F;
		for (int i = 0; i < 3; ++ i) {
			vectorProduct += newPoint[i] * oldPoint[i];
			newMold += newPoint[i] * newPoint[i];
			oldMold += oldPoint[i] * oldPoint[i];
		}
		newMold = (float)Math.sqrt(newMold);
		oldMold = (float)Math.sqrt(oldMold);
		//to calculate cos of angle
		float cosinAngle = (float)(vectorProduct / (newMold * oldMold));
		//to calculate the angle
		float fangle = (float)Math.toDegrees(Math.acos(cosinAngle));
		
		return fangle;
	}
	
	private JudgeService parent;
	private long timePrevious = -1L;
	private float[] preAccData = null;
	private static final float WALKING_THRESHOLD = 30F;

}
