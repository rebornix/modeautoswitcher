package ltlteam.app.rmas;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class JudgeService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	public void showNotification(int id, String text) {
		Notification notification = new Notification(R.drawable.ic_launcher,
				text, System.currentTimeMillis());
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, new Intent(
				this, RingerModeAutoSwitcherActivity.class), 0);
		notification.defaults = Notification.DEFAULT_LIGHTS;
		notification.setLatestEventInfo(this, "情景模式自动切换", text, pIntent);
		nm.notify(id, notification);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		am = (AudioManager) getSystemService(AUDIO_SERVICE);
		curRingerMode = am.getRingerMode();

		// register for sensor listener
		sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
		accSensor = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		lsn = new WalkingListener(this);
		sensorMgr.registerListener(lsn, accSensor,
				SensorManager.SENSOR_DELAY_UI);

		// start audio record thread
		audioRecordThread = new AudioRecordThread(this);
		audioRecordThread.startRecording();
		audioRecordThread.start();

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
//				记算平均样本幅度
				Log.d(TAG,
						String.valueOf((double) volumnSum
								/ (TIME_INTERVAL * SAMPLE_RATE_IN_HZ)));
				if ((float) step / TIME_INTERVAL >= STEP_THRESHOLD) {
					if (curRingerMode != AudioManager.RINGER_MODE_NORMAL) {
						am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
						curRingerMode = AudioManager.RINGER_MODE_NORMAL;
						showNotification(0, "已切换至正常模式");
					}
				} else {
					if (curRingerMode != AudioManager.RINGER_MODE_VIBRATE) {
						am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
						curRingerMode = AudioManager.RINGER_MODE_VIBRATE;
						showNotification(0, "已切换至振动模式");
					}
				}
				step = 0;
				volumnSum = 0;

				Message m = obtainMessage();
				sendMessageDelayed(m, TIME_INTERVAL * 1000);
			}
		};
		Message msg = handler.obtainMessage();
		handler.sendMessageDelayed(msg, TIME_INTERVAL * 1000);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		sensorMgr.unregisterListener(lsn, accSensor);
		audioRecordThread.stopRecording();

	}

	public void addStep() {
		++step;
		Intent intent = new Intent(
				"zossin.app.rmas.RingerModeAutoSwitcherActivity");
		intent.putExtra("step", step);
		sendBroadcast(intent);
	}

	public void addVolumn(int volumn) {
		this.volumnSum += Math.abs(volumn);
	}

	private SensorManager sensorMgr;
	private Sensor accSensor;
	private SensorEventListener lsn;
	private AudioRecordThread audioRecordThread;
	private volatile int step = 0;
	private volatile long volumnSum = 0;
	private static final int TIME_INTERVAL = 30;
	private static final float STEP_THRESHOLD = 1F;
	public static final int SAMPLE_RATE_IN_HZ = 44100;
	private int curRingerMode;
	private AudioManager am;
	private NotificationManager nm;
	private static final String TAG = "JudgeService";

}