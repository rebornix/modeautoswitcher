package ltlteam.app.rmas;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

public class AudioRecordThread extends Thread {

	public AudioRecordThread(JudgeService parent) {
		this.parent = parent;
		int recBufferSize = AudioRecord.getMinBufferSize(
				JudgeService.SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT);
		ar = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
				JudgeService.SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT, 2 * recBufferSize);
	}

	public void startRecording() {
		ar.startRecording();
		isRecording = true;
	}

	public void run() {
		short[] buffer = new short[1];

		while (isRecording) {
			if (ar.read(buffer, 0, 1) > 0) {
//				音频样本的高字节部分是幅度
				parent.addVolumn(buffer[0] >> 8);
			}
		}
	}

	public void stopRecording() {
		isRecording = false;
		ar.stop();
		ar.release();
	}

	private JudgeService parent;
	private boolean isRecording;
	private AudioRecord ar;
	private static final String TAG = "AudioRecordThread";

}
